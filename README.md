# Team: MayTheForceBeWithUs

### Member
- Phumin Chanthalert (612115006)
- Yaxin Su (602115520)

### Frontend
<a href="http://18.206.119.126:8101/" target="_blank">http://18.206.119.126:8101/</a>

### Backend
- Host: **18.206.119.126**
- Port: **8100**

### Backend Repository
<a href="https://gitlab.com/phoomin2012/se234-project">https://gitlab.com/phoomin2012/se234-project</a>

### Katalon Test Repository
<a href="https://gitlab.com/phoomin2012/se234-project-katalon-test">https://gitlab.com/phoomin2012/se234-project-katalon-test</a>